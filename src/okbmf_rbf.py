'''
    Online kernel matrix factorization
'''


__authors__ = 'Jose Bermeo, Miguel Diaz'
__email__ = 'jdbermeol@unal.edu.co, machitivad@unal.edu.co'


from theano import _asarray,tensor, function, shared, config, scan
import numpy

class OKBMF():

    def __init__(self, data, budget, W, learning_rate, sigma, _lambda, alpha):

        self.data = data

        self._num_samples, _ = data.shape

        self.sigma=shared(_asarray(sigma, dtype=config.floatX),
                        name='sigma', borrow=True)

        self.alpha=shared(_asarray(alpha, dtype=config.floatX),
                                        name='alpha', borrow=True)

        self._lambda=shared(_asarray(_lambda, dtype=config.floatX),
                                        name='lambda', borrow=True)
        
        self._compute_kernel_matrices(budget)

        self.num_samples, _ = data.shape

        self.W = shared(_asarray(W, dtype=config.floatX),
                        name='W', borrow=True)

        self.learning_rate = shared(_asarray(learning_rate, dtype=config.floatX),
                        name='learning_rate', borrow=True)

        self._compute_update_rule()
        self._compute_helper_functions()


    def _compute_kernel_matrices(self, budget):
        self.K = shared(_asarray(numpy.ones((1, 1)), dtype=config.floatX),
                        name='K', borrow=True)
        self.KBX = shared(_asarray(numpy.ones((1, 1)), dtype=config.floatX),
                          name='KBX', borrow=True)

        x = tensor.matrix(name='x')
        y = tensor.matrix(name='y')
        idx_x=tensor.ivector(name='idx_x')
        idx_y=tensor.ivector(name='idx_y')
  
  
        result_K, updates = scan(fn=lambda idx1,idx2:
                tensor.exp( -1.0 / (2.0 * self.sigma **2)*(x[idx1]-x[idx2]).norm(L=2,axis=1)),
                sequences=[idx_x], non_sequences=[idx_x])
        result_KBX, updates = scan(fn=lambda idx1,idx2:
                tensor.exp( -1.0 / (2.0 * self.sigma **2)*(x[idx1]-y[idx2]).norm(L=2,axis=1)),
                sequences=[idx_x], non_sequences=[idx_y])

        
        gaussian_kernel=function([idx_x,idx_y], updates=[(self.K,result_K),(self.KBX,result_KBX)],
                givens={x:budget,y:self.data}, name='gaussian_kernel')

        gaussian_kernel(range(budget.shape[0]),range(self.data.shape[0]) )


    def _compute_update_rule(self):
        h = tensor.matrix(name='h')
        index = tensor.lvector(name='index')
        kbx = tensor.matrix(name='kbx')

        a = tensor.dot(tensor.dot(self.W, self.K), self.W.T)
        b = tensor.dot(self.W, kbx)
        self._get_H = function([index], [a, b], givens={kbx: self.KBX[:, index]})

        a = tensor.dot(tensor.dot(tensor.dot(h, h.T), self.W), self.K)
        step = self.learning_rate / (index.shape[0].astype(a.dtype))
        a = step * a
        b = step * tensor.dot(h, kbx.T)
        c = tensor.dot(self._lambda*self.W , self.K) - a + b
        self._update_W = function([index, h], [], updates=[(self.W, c)],
                                  givens={kbx: self.KBX[:, index]})

    def _compute_helper_functions(self):
        h = tensor.matrix(name='h')
        index = tensor.lvector(name='index')
        kbx = tensor.matrix(name='kbx')
        x = tensor.matrix(name='x')
        

        #a = tensor.dot(kbx, h.T)
        #b = tensor.dot(self.K,self.W.T)
        #c = -tensor.dot(b,tensor.dot(h,h.T))
        #b= self._lambda*b
        #d = tensor.dot(x, x.T) + a +  b + c #frobenius 2-norm

        a = -2 * tensor.dot(h.T, tensor.dot(self.W, kbx))
        b = tensor.dot(h.T, self.W)
        c = tensor.dot(tensor.dot(b, self.K), b.T)
        d = (self.K + a + c).trace()


        self.error = function([x, index, h], d,on_unused_input='ignore',
                              givens={kbx: self.KBX[:, index]})

    def _update_learning_rate(self, decay):
        if self._count == .0:
            self._base = self.learning_rate.get_value()
        self._count += 1.0

        learning_rate = .0

        if decay == 0.0:
            learning_rate = self._base / self._count
        else:
            learning_rate = self._base * decay / (decay + self._count)

        self.learning_rate.set_value(float(learning_rate))

    def _compute_current_error(self, batch_size):
        current_error = .0
        for j in xrange(0, self._num_samples, batch_size):
            batch = range(j, min(j + batch_size, self._num_samples))
            a, b = self._get_H(batch)
            h = numpy.linalg.solve(a, b)
            current_error += self.error(self.data[batch], batch, h)
            #print .5*current_error/self._num_samples
        return .5 * current_error / (self._num_samples + .0)

    def train_all(self, epocs=10, error=.0, batch_size=10, decay=.0):

        self._count = .0
        for i in xrange(epocs):
            permutation = numpy.random.permutation(self._num_samples)
            for j in xrange(0, self._num_samples, batch_size):
                batch = permutation[j: min(j + batch_size, self._num_samples)]
                batch.sort()
                a, b = self._get_H(batch)
                h = numpy.linalg.solve(a, b)
                print a.shape, b.shape, h.shape

                #print numpy.allclose(numpy.dot(a, h), b,  rtol=1e-02, atol=1e-05)

                self._update_W(batch, h)
                self._update_learning_rate(float(decay))
        return self._compute_current_error(batch_size)
