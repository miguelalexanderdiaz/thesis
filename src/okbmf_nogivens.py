'''
    Online kernel matrix factorization
'''


__authors__ = 'Jose Bermeo, Miguel Diaz'
__email__ = 'jdbermeol@unal.edu.co, machitivad@unal.edu.co'


from theano import _asarray
from theano import config
from theano import tensor
from theano import function
from theano import shared
import numpy


class OKBMF():

    def __init__(self, data, budget, W, learning_rate):

        self.data = data

        self._num_samples, _ = data.shape

        self._compute_kernel_matrices(budget)

        self.num_samples, _ = data.shape

        self.W = shared(_asarray(W, dtype=config.floatX),
                        name='W', borrow=True)

        self.learning_rate = shared(_asarray(learning_rate, dtype=config.floatX),
                        name='learning_rate', borrow=True)

        self._compute_update_rule()
        self._compute_helper_functions()

    def _compute_kernel_matrices(self, budget):
        
        self.K = shared(_asarray(numpy.ones((1, 1)), dtype=config.floatX),
                        name='K', borrow=True)
        self.KBX = shared(_asarray(numpy.ones((1, 1)), dtype=config.floatX),
                          name='KBX', borrow=True)

        x = tensor.matrix(name='x')
        y = tensor.matrix(name='y')

        kernel_function = function(inputs=[x,y], updates=[(self.K, tensor.dot(x, x.T)), (self.KBX, tensor.dot(x, y.T))])
        kernel_function(budget,self.data)

    def _compute_update_rule(self):
        h = tensor.matrix(name='h')
        index = tensor.lvector(name='index')
        kbx = tensor.matrix(name='kbx')
        kbx= self.KBX[:, index]
        a = tensor.dot(tensor.dot(self.W, self.K), self.W.T)
        b = tensor.dot(self.W, kbx)
        self._get_H = function([index], [a, b])

        a = tensor.dot(tensor.dot(tensor.dot(h, h.T), self.W), self.K)
        step = self.learning_rate / (index.shape[0].astype(a.dtype))
        a = step * a
        b = tensor.dot(h, kbx.T)
        b = step * b
        c = self.W - a + b
        kbx=self.KBX[:, index]
        self._update_W = function([index, h], [], updates=[(self.W, c)])

    def _compute_helper_functions(self):
        h = tensor.matrix(name='h')
        index = tensor.lvector(name='index')
        kbx = tensor.matrix(name='kbx')
        x = tensor.matrix(name='x')
        
        kbx=self.KBX[:, index]
        a = -2 * tensor.dot(h.T, tensor.dot(self.W, kbx))
        b = tensor.dot(h.T, self.W)
        c = tensor.dot(tensor.dot(b, self.K), b.T)
        d = (tensor.dot(x, x.T) + a + c).trace()
        self.error = function([x, index, h], d)

    def _update_learning_rate(self, decay):
        if self._count == .0:
            self._base = self.learning_rate.get_value()
        self._count += 1.0

        learning_rate = .0

        if decay == 0.0:
            learning_rate = self._base / self._count
        else:
            learning_rate = self._base * decay / (decay + self._count)

        self.learning_rate.set_value(float(learning_rate))

    def _compute_current_error(self, batch_size):
        current_error = .0
        for j in xrange(0, self._num_samples, batch_size):
            batch = range(j, min(j + batch_size, self._num_samples))
            a, b = self._get_H(batch)
            h = numpy.linalg.solve(a, b)
            current_error += self.error(self.data[batch], batch, h)
        return .5 * current_error / (self._num_samples + .0)

    def train_all(self, epocs=10, error=.0, batch_size=10, decay=.0):

        self._count = .0
        for i in xrange(epocs):
            permutation = numpy.random.permutation(self._num_samples)
            for j in xrange(0, self._num_samples, batch_size):
                batch = permutation[j: min(j + batch_size, self._num_samples)]
                batch.sort()
                a, b = self._get_H(batch)
                h = numpy.linalg.solve(a, b)
                self._update_W(batch, h)
                self._update_learning_rate(decay)

        return self._compute_current_error(batch_size)
