#givens

from src.okbmf import OKBMF
import tables
import numpy
import time


# Load data
MNIST_PATH="/home/migueldiaz/Documentos/Thesis/Data/MNIST/mnist.h5"
dataset_table = tables.open_file(MNIST_PATH, "r")
dataset = dataset_table.get_node("/train_set/data")[:]
dataset_table.close()

# Set constants
_, num_features = dataset.shape
num_latent_topics = 100

# Warm-up
#data = dataset[:1000, :]
#W = 1.2 * numpy.random.rand(num_latent_topics, 1000)

#okbmf = OKBMF(data, data, W, 100)
#error = okbmf.train_all(epocs=1, batch_size=1000, decay=1000)
#del okbmf

start = int(round(time.time() * 1000))


#for budget_size in xrange(100, 4001, 100):
budget_size=1000

print "Budget size: " + str(budget_size)
budget = dataset[:budget_size, :]
W = 1.2 * numpy.random.rand(num_latent_topics, budget_size)

leap = int(round(time.time() * 1000))

okbmf = OKBMF(dataset, budget, W, 100)

print "Compile time: " + str(int(round(time.time() * 1000)) - leap)

leap = int(round(time.time() * 1000))

error = okbmf.train_all(epocs=5, batch_size=1000, decay=1000)

print "Error: " + str(error) + " Execution time: " + str(int(round(time.time() * 1000)) - leap)
del okbmf


print "\n\n Total time:" + str(int(round(time.time() * 1000)) - start)

#print theano.config

